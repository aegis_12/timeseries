
import React, {Component} from 'react'
import PropTypes from 'prop-types';

export default class Bar extends Component {
    render() {
        let {data, size, style} = this.props;
        let {color} = style;
        let {height} = size;
        
        // cambiar en otro sitio
        let columnWidth = 15

        return (
            <g>
                {data.map((point, index) => {
                    let {x, y} = point;
                    return <rect
                        key={index}
                        x={x-columnWidth/2}
                        y={y}
                        width={columnWidth}
                        fill={color}
                        height={height-y}
                    />
                })}
            </g>
        )
    }
}
