
import React, {Component} from 'react'
import PropTypes from 'prop-types';

var range = require('lodash.range');

function linspace(a,b,n) {
    var every = (b-a)/(n-1),
    ranged = range(a,b,every);
    
    return ranged.length == n ? ranged : ranged.concat(b);
}

function createIntervalTicks(min, max, num) {
    return linspace(min, max, num)
}

function createTicks(min, max, interval) {
    let val = [];
    for (var i=min; i<max; i+=interval) {
        console.log(i)
        val.push(i)
    }
    return val
}

export default class AxisY extends Component {
    render() {
        let {axis,size} = this.props;
        let {range, domain} = axis;

        let {width} = size;
        let ticks = createIntervalTicks(range[0], range[1], 5);

        return (
            <g>
                {ticks.map(tick => {
                    let pos = axis.interpolate(tick)

                    return (
                        <g
                            key={tick}
                        >
                            <line
                                y1={pos}
                                x1="0" 
                                y2={pos} 
                                x2={width}
                                style={{stroke: '#555555', strokeWidth:1}}
                            />
                            <text
                                x={0}
                                y={pos}
                            >
                                {'CC'}
                            </text>
                        </g>
                    )
                })}
            </g>
        )
    }
}
