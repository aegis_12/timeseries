
import React, {Component} from 'react'
import PropTypes from 'prop-types';

export class Path extends Component {
    render() {
        let {path, style} = this.props;
        let {color} = style;
        
        return (
            <path 
                d={path}
                stroke={color}
                strokeWidth={3}
                fill={'none'}
            />
        )
    }
}

Path.propTypes = {
    path: PropTypes.string,
    style: PropTypes.shape({
        stroke: PropTypes.string,
        strokeWidth: PropTypes.string,
        fill: PropTypes.string
    })
}
