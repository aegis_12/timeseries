
import React, {Component} from 'react'
import PropTypes from 'prop-types';

import {Path} from './Path';
import {curveInterpolate} from '../lib/curve'

export default class Line extends Component {
    render() {
        let {data, curve, style} = this.props;
        let pathdata = curveInterpolate(curve, data)
        
        return (
            <Path 
                path={pathdata}
                style={style}
            />
        )
    }
}
