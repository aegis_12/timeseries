
import React, {Component} from 'react'
import PropTypes from 'prop-types';

const Circle = ({id, radius, color, stroke}) => (
    <circle
        id={id}
        r={5}
        fill={color}
        stroke={stroke}
        strokeWidth={3}
    />
)

export default class Dots extends Component {
    render() {
        let {data, style} = this.props;
        
        return (
            <g>
                <defs>
                    <Circle id={'circle'} {...style} />
                </defs>
                {data.map((point, index) => {
                    let {x, y} = point;
                    return (
                        <use key={index} xlinkHref={"#circle"} x={x} y={y} />
                    )
                })}
            </g>
        )
    }
}
