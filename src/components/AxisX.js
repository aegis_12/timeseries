
import React, {Component} from 'react'
import PropTypes from 'prop-types';

import {HOUR, MINUTE} from '../lib/props'

function roundToN(x, n){
    return Math.ceil(x/n)*n;
}

function createRoundTicks(min, max, round) {
    let minimal = roundToN(min, round) - round;
    let maximal = roundToN(max, round) + round;

    let values = [];
    for (var i = minimal; i<maximal; i+=round) {
        values.push(i)
    }

    return values
}



export default class AxisX extends Component {
    render() {
        let {axis, size} = this.props;
        let {range} = axis;
        
        let {height} = size;
        let ticks = createRoundTicks(range[0], range[1], MINUTE)

        return (
            <g>
                {ticks.map((tick, index) => {
                    let pos = axis.interpolate(tick)

                    return (
                        <g
                            key={index}
                        >
                            <line
                                x1={pos}
                                y1={'0'}
                                x2={pos}
                                y2={height}
                                style={{stroke: '#555555', strokeWidth:1}}
                            />
                            <text
                                x={pos}
                                y={height}
                            >
                                {'ABC'}
                            </text>
                        </g>
                    )
                })}
            </g>
        )
    }
}
