
// Base de estilos para las lineas

import ColorScheme from 'color-scheme'

export function styleGenerator(hue) {
    let scheme = new ColorScheme;

    scheme.from_hue(hue)         
    .scheme('mono')   
    .variation('default')

    let colors = scheme.colors();

    return {
        line: {color: '#'+colors[0]},
        dots: {color: '#'+colors[1], stroke: '#'+colors[2]},
        bars: {color: '#'+colors[3]}
    }
}

export const styles = [
    styleGenerator(10),
    styleGenerator(100),
    styleGenerator(200)
]
