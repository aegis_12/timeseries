
import { createDevTools } from 'redux-devtools'
import LogMonitor from 'redux-devtools-log-monitor'
import DockMonitor from 'redux-devtools-dock-monitor'

import React, {Component} from 'react'
import { render } from 'react-dom'

import thunkMiddleware from 'redux-thunk'
import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'

import entities from './redux/reducer'

import api from './redux/middleware'


import Block from './containers/Block'
import Chart from './containers/Chart'

import {HOUR} from './lib/props'

const reducer = combineReducers({
    entities
})

const DevTools = createDevTools(
    <DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-q">
        <LogMonitor theme="tomorrow" preserveScrollTop={false} />
    </DockMonitor>
)

const store = createStore(
    reducer,
    compose (
        applyMiddleware (
            thunkMiddleware,
            api
        ),
        DevTools.instrument()
    )
)

let chart = {
    size: {
        height: 400,
        width: 500
    },
    range: undefined,
    events: [
        {
            name: 'event1',
            view: {
                dots: true,
            }
        },
        {
            name: 'event2',
            view: {
                dots: true
            }
        }
    ]
}

const initialQuery = {
    relative: {
        count: 10,
        which: 'minutes'
    }
}

class App extends Component {
    constructor(props) {
        super(props)

        let {query} = this.props.chart;

        this.state = {
            query: query == undefined ? initialQuery : query
        }

        this.onChangeRange = this.onChangeRange.bind(this)
    }

    onChangeRange(newquery) {
        console.log("-- query --")
        console.log(newquery)

        this.setState({
            query: {
                relative: newquery
            }
        })
    }

    render() {
        let {chart} = this.props;
        let {query} = this.state;

        return (
            <div>
                <Block 
                    {...chart}
                    query={query}
                    onChangeRange={this.onChangeRange} 
                />
            </div>
        )
    }
}

const Root = () => (
    <Provider store={store}>
        <div>
            <App chart={chart} />
            <DevTools />
        </div>
    </Provider>
)

render(
    <Root />,
    document.getElementById('root')
)
