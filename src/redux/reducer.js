
import Pond, {checkOrder} from '../lib/pond/src'
import {LOG_SUCCESS} from './actions'

const initialState = {
    logs: {}
}

function convertResult(result) {
    return Object.keys(result).map(i => {
        let {Milliseconds, Value} = result[i];
        let value = parseInt(Value)

        return [
            Milliseconds,
            value
        ]
    })
}

export default function expand(state = initialState, action) {

    let {type} = action
    if (type === 'LOG_SUCCESS') {
        let {response, id} = action;
        let result = convertResult(response)

        let order = checkOrder(result)
        if (order != true) {
            console.error("El orden no es cronologico")
        }
        
        let x = new Pond()
        x.add(result)
        
        return Object.assign({}, state, {
            [id]: x
        })
    }
    
    return state
}
