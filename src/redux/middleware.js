
import moment from 'moment'

import {LOG_REQUEST, LOG_SUCCESS, LOG_FAILURE} from './actions'

const API_ROOT = 'http://localhost:1324/'

export const CALL_API = Symbol('Call api')

const callApi = (endpoint, period) => {
    const fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? API_ROOT + endpoint : endpoint

    // CONFIGURACION DE TIMEPO DE LA LLAMADA

    let {start, end} = period;

    let data = new FormData()
    data.append('start', start)
    data.append('end', end)

    let config = {
        method: 'POST',
        body: data
    }

    return fetch(fullUrl, config)
    .then(response =>
        response.json().then(json => {
            if (!response.ok) {
                return Promise.reject(json)
            }
            
            return Object.assign({}, json.result)
        }))
}


export default store => next => action => {

    const callAPI = action[CALL_API]
    if (typeof callAPI === 'undefined') {
        return next(action)
    }

    let { endpoint, id, period } = callAPI
    const types = [LOG_REQUEST, LOG_SUCCESS, LOG_FAILURE]
    
    console.log("-- id --")
    console.log(id)

    if (typeof endpoint === 'function') {
        endpoint = endpoint(store.getState())
    }

    if (typeof endpoint !== 'string') {
        throw new Error('Specify a string endpoint URL.')
    }
    if (!Array.isArray(types) || types.length !== 3) {
        throw new Error('Expected an array of three action types.')
    }
    if (!types.every(type => typeof type === 'string')) {
        throw new Error('Expected action types to be strings.')
    }

    const actionWith = data => {
        const finalAction = Object.assign({}, action, data)
        delete finalAction[CALL_API]
        return finalAction
    }

    const [ requestType, successType, failureType ] = types
    next(actionWith({ type: requestType }))

    return callApi(endpoint, period).then(
        response => next(actionWith({
            response,
            type: successType,
            id: id
        })),
        error => next(actionWith({
            type: failureType,
            id: id,
            error: error.message || 'Something bad happened'
        }))
    )
}
