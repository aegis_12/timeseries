
import { CALL_API } from './middleware'

export const LOG_REQUEST = 'LOG_REQUEST'
export const LOG_SUCCESS = 'LOG_SUCCESS'
export const LOG_FAILURE = 'LOG_FAILURE'

// Generico para todos los dispositivos

const fetchLog = (event, period) => ({
    [CALL_API]: {
        endpoint: `logs/${event}`,
        id: event,
        period: period
    }
})

export const loadLog = (event, period) => (dispatch, getState) => {     // deberia gestionar que el periodo que se pregunta no este repetido...
    dispatch(fetchLog(event, period))
}
