
// fa ja la gestio completa del chart. desplegar les series en un svg y actualizarles.

import React, {Component} from 'react'
import {connect} from 'react-redux'

// chart crea el tools tame, aixina quan tools actualize que faja un setState a series

import {Axis, AxisG} from '../lib/axis'

import Series from './Series'
import AxisX from '../components/AxisX'
import AxisY from '../components/AxisY'

import Tools from './Tools'
import Legend from './Legend'

function toMiliseconds(period) {
    let {start, end} = period;

    return {
        'start': start.getTime(),
        'end': end.getTime()
    }
}

function makePoint(t, axisX, axisY) {
    return {
        x: axisX.interpolate(t[0]),
        y: axisY.interpolate(t[1])
    }
}

class SelRect extends Component {
    render() {
        let {initial, current, size} = this.props;
        let {height} = size;

        let style = {stroke: '#b1ebf7', fill: '#6cc5ff', 'fill-opacity': 0.2, 'shape-rendering': 'crispEdges'}

        if (initial <= current) {
            return <rect x={initial} y={0} width={current-initial} height={height} style={style}/>
        }
        if (current < initial) {
            return <rect x={current} y={0} width={initial-current} height={height} style={style}/>
        }
    }
}

const Loading = ({width, height}) => (
    <div style={{width: width, height: height, backgroundColor: 'blue'}}>
        {'Loading'}
    </div>
)

export default class Chart extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        let {query, ts, size, events, period, realtime} = this.props;

        let {start, end} = period;
        let {width, height} = size;
        
        // Pixel por minuto
        let resta = end - start;
        let minutes =  ((resta / (1000*60)) % 60);

        let perMinute = width / minutes;

        // Esto a lo millor soles en el graph?? 
        let axisX = new AxisG([start, end], perMinute)
        let axisY = new Axis([0, height], [0, 15])
        
        // sacar cada ts
        let slicets = ts.map(t => t.slice(start, end))
        let normats = slicets.map(t => t.map(i => makePoint(i, axisX, axisY)))

        // crear las series con cada uno
        let series = normats.map((n, index) => <Series key={index} event={events[index]} period={period} size={size} realtime={realtime} data={n} />)

        // a partir del range calcular el transform

        return (
            <div>
                <svg
                    width={width}
                    height={height}
                >
                    <g>
                        {series}
                        <AxisX axis={axisX} size={size} /> 
                        <AxisY axis={axisY} size={size} />
                    </g>
                    <Legend
                    />
                </svg>
                <Tools
                    query={query}
                    onChangeRange={this.props.onChangeRange}
                />
            </div>
        )
    }
}
