
import React, {Component} from 'react'
import {connect} from 'react-redux'

import merge from 'lodash.merge'

import moment from 'moment'

import Chart from './Chart'
import Tools from './Tools'
import {styles, styleGenerator} from '../utils/styles'

import Pond from '../lib/pond/src'

function differentPeriods(oldperiods, nextperiods) {
    let period = oldperiods;

    if (oldperiods.start >= nextperiods.start) {
        return true
    }

    if (oldperiods.end <= nextperiods.end) {
        return true
    }

    return false
}

class Block extends Component {
    constructor(props) {
        super(props)

        this.state = {
            period: props.period,    // periodo de tiempo,
            range: props.period      // rango de tiempo visual
        }

        this.move = 0
    }
    
    componentWillReceiveProps(nextprops) {
        /*
        if (differentPeriods(this.props.period, nextprops.period)) {
            console.log("--- PROPS differente ---")
            console.log(nextprops.period)
        }
        */
    }

    render() {
        let {period, range} = this.state;
        
        return (
            <div>
                <Chart 
                    {...this.props} 
                    period={period}
                    range={range}
                    onChangeRange={this.props.onChangeRange}
                />
            </div>
        )
    }
}

const initialView = {
    line: true,
    dots: false,
    bars: false
}


const initialLog = new Pond()

function composePeriod(period) {
    let {absolute, relative} = period;

    if (absolute && relative) {
        console.error("Estan absolute y relative a la vez")
    }

    if (!absolute && !relative) {
        console.error("No hay ni absolute ni relative")
    }

    if (absolute) {
        return absolute // porque aqui ya tiene el start, end
    }

    if (relative) {
        let {count, which} = relative;

        let start = moment().subtract(count, which).valueOf()
        let end = moment().valueOf()

        return {
            start: start,
            end: end
        }
    }
}

const mapStateToProps = (state, ownProps) => {
    let {query, events} = ownProps;

    // componer el periodo
    let period = composePeriod(query)

    // sacar las series temporales
    let ts = events.map(event => state.entities[event.name] || initialLog)

    events = events.map((event, index) => {
        let defaultStyle = styles[index]

        return Object.assign({}, event, {
            view: merge({}, initialView, event.view),       // views en caso de que falte alguno
            style: merge({}, defaultStyle, event.style)
        })
    })
    
    return {
        ts: ts,
        events: events,
        period: period,
    }
}

const mapDispatchToProps = (dispatch, getState) => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Block)
