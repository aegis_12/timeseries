
// Tools esta a un costat para cambiar les propietats de les series temporals

import React, {Component} from 'react'

const whichs = [
    'seconds',
    'minutes',
    'hours'
]

const Selector = ({value, onChange}) => (
    <select
        value={value}
        onChange={(e) => onChange(e.target.value)}
    >
        {whichs.map((item, key) => (
            <option
                key={key}
                value={item}
            >
                {item}
            </option>
        ))}
    </select>
)

const Number = ({value, onChange}) => (
    <input
        type={'number'}
        value={value}
        onChange={(e) => onChange(parseInt(e.target.value))}
    />
)

class TimeRange extends Component {
    render() {
        let {count, which} = this.props;

        return (
            <div>
                <Number
                    value={count}
                    onChange={(e) => {
                        this.props.onChange({count: e, which: which})
                    }}
                />
                <Selector 
                    value={which}
                    onChange={(e) => {
                        this.props.onChange({count: count, which: e})
                    }}
                />
            </div>
        )
    }
}

export default class Tools extends Component {
    render() {
        let {query} = this.props;
        let {relative} = query;

        console.log("--- props ----")
        console.log(query)

        return (
            <div>
                Tools
                <TimeRange 
                    {...relative} 
                    onChange={this.props.onChangeRange}
                />
            </div>
        )
    }
}
