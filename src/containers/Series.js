
// Cada una de las series temporales en el chart
// Se le dice si es un barchart o line o dots

// series es el que tiene que gestionar el ciclo de vida, hacer las llamadas y todo lo demas no?

import React, {Component} from 'react'
import {connect} from 'react-redux'

import {loadLog} from '../redux/actions'
import Line from '../components/Line'
import Dots from '../components/Dots'
import Bar from '../components/Bar'

function differentPeriods(oldperiods, nextperiods) {
    let period = oldperiods;

    if (oldperiods.start >= nextperiods.start) {
        return true
    }

    if (oldperiods.end <= nextperiods.end) {
        return true
    }

    return false
}

class Series extends Component {
    constructor(props) {
        super(props)
    }

    componentWillReceiveProps(nextprops) {
        let {event} = this.props;
        let {name} = event;
        if (differentPeriods(this.props.period, nextprops.period)) {
            //this.props.loadLog(name, nextprops.period)
        }
    }
    
    componentDidMount() {
        let {event, period} = this.props;
        let {name} = event;
        
        this.props.loadLog(name, period)
    }
    
    render() {
        let {data, size, event} = this.props;
        let {view, style} = event;

        return (
            <g>
                {view.line &&
                    <Line style={style.line} data={data} curve={'linear'} />
                }
                {view.dots &&
                    <Dots style={style.dots} data={data} />
                }
                {view.bars &&
                    <Bar style={style.bars} data={data} size={size} />
                }
            </g>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {

    }
}

const mapDispatchToProps = (dispatch, getState) => {
    return {
        loadLog: function(event, period) {
            dispatch(loadLog(event, period))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Series)
