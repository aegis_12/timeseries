
import {List} from 'immutable'

export function checkOrder(items) {
    if (items.length == 0) {
        return true
    }

    for (var i = 1; i < items.length; i++) {
        if (items[i-1][0] > items[i][0]) {
            return i
        }
    }

    return true
}

/**
 *  Pond es la clase para gestionar la serie temporal
 */

class Pond {
    constructor() {
        this.list = List()
    }

    _findEnd(timestamp) {
        for (let i = this.length() - 1; i > 0; i--) {
            if (timestamp >= this.list.get(i)[0]) {
                return i
            }
        }
        return -1
    }
    
    _findStart(timestamp) {
        for (let i = 0; i < this.length(); i++) {
            if (timestamp <= this.list.get(i)[0]) {
                return i
            }
        }
        return -1
    }
    
    find(timestamp) {   // encontrar timestamp (exacto) con binary search
        if (this.empty()) {
            return -1
        }

        let minIndex = 0;
        let maxIndex = this.length() - 1;

        let currentIndex;
        let currentElement;

        while (minIndex <= maxIndex) {
            currentIndex = (minIndex + maxIndex) / 2 | 0;
            currentElement = this.list.get(currentIndex)

            let value = currentElement[0]
            if (value < timestamp) {
                minIndex = currentIndex + 1
            }
            else if (value > timestamp) {
                maxIndex = currentIndex - 1;
            }
            else {
                return currentIndex
            }
        }

        return -1
    }

    // slice llama a fechas
    slice(start, end, margin=10) {  // margin son los items a cada lado que se dejan

        let startindex = this._findStart(start)
        let endindex = this._findEnd(end)

        if (startindex == -1 || endindex == -1) {
            return []
        }

        startindex = (startindex - margin < 0) ? 0 : startindex - margin;
        endindex   = (endindex + margin > this.length()) ? this.length() : endindex + margin;

        return this.list.slice(startindex, endindex+1).toArray()
    }
    
    // El array se almacena de viejo a nuevo
    // ... i-2, i-1, i, i+1, i+2 ...

    add(items) {    // que sea siempre un array
        if (items.length == 0) {
            return 
        }

        let order = checkOrder(items)
        if (order != true) {
            console.error("El orden no es correcto en add. Fallo en el indice " + order)
        }
        
        if (this.empty()) {
            this.list = List(items)    
        } else {
            
        }
    }

    toArray() {
        let array = this.list.toArray()
        
        let order = checkOrder(array)
        if (order != true) {
            console.error("El orden no es correcto en toArray. Fallo en el indice " + order)
        }

        return array
    }
    
    first() {
        return this.empty() ? undefined: this.list.get(0)
    }
    
    last() {
        let len = this.length()
        return len == 0 ? undefined: this.list.get(len - 1)
    }
    
    empty() {
        return this.length() == 0
    }

    length() {
        return this.list.size
    }
}

export default Pond
