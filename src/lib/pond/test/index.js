
import {expect} from 'chai'
import Pond, {checkOrder} from '../src'

describe('Check order', () => {
    
    it('Good order', () => {
        expect(checkOrder([[122], [123], [124]])).to.equal(true)
    })

    it('Bad order', () => {
        expect(checkOrder([[122], [123], [121]])).to.equal(2)
    })

})

describe('Crear pondjs (basico)', () => {

    it('Vacio', () => {
        let pond = new Pond()
        expect(pond.length()).to.equal(0)
        expect(pond.toArray()).to.deep.equal([])

        expect(pond.empty()).to.equal(true)
        expect(pond.first()).to.be.undefined;
        expect(pond.last()).to.be.undefined;
    })
    
    it('Add primer array', () => {
        let data = [[123,3], [124,4], [125,5]]

        let pond = new Pond();
        pond.add(data)

        expect(pond.length()).to.equal(3)
        expect(pond.toArray()).to.deep.equal(data)
        expect(pond.first()).to.deep.equal(data[0]);
        expect(pond.last()).to.deep.equal(data[2]);
    })
    
    it('Find (binary search) empty', () => {
        let pond = new Pond()
        pond.add([])

        expect(pond.find(124)).to.equal(-1)
    })

    it('Find (binary search) complete', () => {
        let data = [[123,3], [124,4], [125,5]]

        let pond = new Pond();
        pond.add(data)

        expect(pond.find(124)).to.equal(1)
        expect(pond.find(125)).to.equal(2)
    })

    it('Find (first and end)', () => {
        let data = [[123,3], [124,4], [125,5], [127, 6], [128, 7]]

        let pond = new Pond();
        pond.add(data)

        expect(pond._findStart(124)).to.equal(1)
        expect(pond._findEnd(126)).to.equal(2)

        expect(pond._findStart(129)).to.equal(-1)
        expect(pond._findEnd(135)).to.equal(4)
        expect(pond._findEnd(128)).to.equal(4)
    })

    it('Find array', () => {
        let data = [[123,3], [124,4], [125,5], [127, 6], [128, 7]]

        let pond = new Pond();
        pond.add(data)
        
        // rango justo y rango fuera
        expect(pond.slice(123, 128)).deep.equal([[123,3], [124,4], [125,5], [127, 6], [128, 7]])
        expect(pond.slice(120, 130)).deep.equal([[123,3], [124,4], [125,5], [127, 6], [128, 7]])
        
        // rango dentro
        expect(pond.slice(124, 127)).deep.equal([[124,4], [125,5], [127, 6]])

        // rango dentro hasta limite derecha y fuera
        expect(pond.slice(124, 128)).deep.equal([[124,4], [125,5], [127, 6], [128, 7]])
        expect(pond.slice(124, 130)).deep.equal([[124,4], [125,5], [127, 6], [128, 7]])

        // rango dentro hasta limite izquierda y fuera
        expect(pond.slice(120, 125)).deep.equal([[123,3], [124,4], [125,5]])
        expect(pond.slice(123, 125)).deep.equal([[123,3], [124,4], [125,5]])

    })

})
