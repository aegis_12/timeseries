'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _immutable = require('immutable');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Utilidades
// isArray

function isArray(o) {
    return Object.prototype.toString.call(o) === '[object Array]';
}

/**
 *  Pond es la clase para gestionar la serie temporal
 */

var Pond = function () {
    function Pond() {
        _classCallCheck(this, Pond);

        this.list = (0, _immutable.List)();
    }

    _createClass(Pond, [{
        key: 'add',
        value: function add(items) {
            if (!isArray(items)) {
                items = [items];
            }

            if (this.empty()) {
                this.list = (0, _immutable.List)(items);
            } else {}
        }
    }, {
        key: 'toArray',
        value: function toArray() {
            return this.list.toArray();
        }
    }, {
        key: 'first',
        value: function first() {
            return this.empty() ? undefined : this.list.get(0);
        }
    }, {
        key: 'last',
        value: function last() {
            var len = this.length();
            return len == 0 ? undefined : this.list.get(len - 1);
        }
    }, {
        key: 'empty',
        value: function empty() {
            return this.length() == 0;
        }
    }, {
        key: 'length',
        value: function length() {
            return this.list.size;
        }
    }]);

    return Pond;
}();

exports.default = Pond;