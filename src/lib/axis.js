
// Libreria para los axis y las interpolaciones

// Cada axis

export class Axis {
    constructor(domain, range) {
        this.domain = domain
        this.range = range
    }

    getRange() {
        let {range} = this;
        return {min: range[0], max: range[1]}
    }

    getDomain() {
        let {domain} = this;
        return {min: domain[0], max: domain[1]}
    }

    reverse(pixel) {
        let {domain, range} = this;
        return range[0] + (pixel - domain[0]) * (range[1] - range[0]) / (domain[1] - domain[0])
    }

    interpolate(point) {
        let {domain, range} = this;
        let value = domain[0] + (point - range[0]) * (domain[1] - domain[0]) / (range[1] - range[0]);
        return value
    }
}

export class AxisG extends Axis {
    constructor(range, perMinute) {
        let start = range[0]
        let end = range[1]
        
        let diff = end - start;

        let minutes =  ((diff / (1000*60)) % 60);

        console.log("-- minutes --")
        console.log(minutes)

        super([0, minutes*perMinute], range)
    }
}
