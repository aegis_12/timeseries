
// libreria para interpolar segun el algoritmo en d3-shape

import {curveCardinal, curveLinear, curveNatural, curveStep} from 'd3-shape';
import {path} from "d3-path";

const curves = {
    'cardinal': curveCardinal,
    'linear': curveLinear,
    'natural': curveNatural,
    'curveStep': curveStep,
}

export function getCurves() {
    return Object.keys(curves)
}

export function curveInterpolate(curveName, data) {
    let curve = curves[curveName]
    if (curve == undefined) {
        console.error("El nombre de la curva " + curveName + " no existe")
    }

    let buffer;
    let output = new curve(buffer = path());

    output.lineStart();
    data.map(item => {
        let {x, y} = item;
        output.point(x, y)
    })
    output.lineEnd()

    return buffer.toString()
}
