package main

import (
	"fmt"
	"log"
	"net/http"

	"time"

	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	_ "github.com/mattn/go-sqlite3"
)

var DB *sqlx.DB

func init() {
	db, err := sqlx.Connect("sqlite3", "/tmp/timeseries_test.db")
	if err != nil {
		log.Fatal(err)
	}

	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}

	DB = db
}

type Log struct {
	Devicename, Event, Value string
	Milliseconds             int
	Timestamp                *time.Time
}

func main() {
	e := echo.New()

	// bd

	e.POST("/logs/:id", func(c echo.Context) error { // falta el device pero es facil de poner.
		id := c.Param("id")

		start := c.FormValue("start")
		end := c.FormValue("end")

		/*
			startTime, err := time.Parse(time.RFC3339, start)
			if err != nil {
				return c.String(http.StatusBadRequest, err.Error())
			}

			endTime, err := time.Parse(time.RFC3339, end)
			if err != nil {
				return c.String(http.StatusBadRequest, err.Error())
			}

			startMilliseconds := startTime.UnixNano() / int64(time.Millisecond)
			endMilliseconds := endTime.UnixNano() / int64(time.Millisecond)

			fmt.Println("------")
			fmt.Println(id)

			fmt.Printf("%s => %s (%d)\n", start, startTime.String(), startMilliseconds)
			fmt.Printf("%s => %s (%d)\n", end, endTime.String(), endMilliseconds)
		*/

		// peticion

		logs := []Log{}

		query := fmt.Sprintf("Select * from log WHERE event='%s' and %s < milliseconds and milliseconds < %s", id, start, end)

		err := DB.Select(&logs, query)
		if err != nil {
			return c.String(http.StatusBadRequest, err.Error())
		}

		// converitr logs en objeto salida

		return c.JSON(http.StatusOK, map[string]interface{}{"Status": "OK", "result": logs})
	})

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))

	e.Logger.Fatal(e.Start(":1324"))
}
