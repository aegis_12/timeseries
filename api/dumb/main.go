package main

import (
	"fmt"
	"log"
	"math/rand"

	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

var DB *sqlx.DB

func init() {
	db, err := sqlx.Connect("sqlite3", "/tmp/timeseries_test.db")
	if err != nil {
		log.Fatal(err)
	}

	if err = db.Ping(); err != nil {
		log.Fatal(err)
	}

	DB = db
}

var schema = `
CREATE TABLE log (
	devicename text,
	event text,
	value text,
	milliseconds bigint,
	timestamp TIMESTAMP
)
`

func main() {

	_, err := DB.Exec(schema)
	if err != nil {
		fmt.Println(err)
	}

	s := rand.NewSource(time.Now().Unix())

	devices := []string{"xaa1", "xaa2"}
	events := []string{"event1", "event2"}
	values := []string{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}

	intervals := []int{3, 6, 10}

	for {

		r := rand.New(s)

		device := devices[r.Intn(len(devices))]
		event := events[r.Intn(len(events))]
		value := values[r.Intn(len(values))]

		now := time.Now()
		milliseconds := now.UnixNano() / int64(time.Millisecond)

		fmt.Println("---------")
		fmt.Println(now)
		fmt.Println(device)
		fmt.Println(event)
		fmt.Println(value)

		DB.Exec("INSERT INTO log(devicename, event, value, milliseconds, timestamp) VALUES ($1, $2, $3, $4, $5)", device, event, value, milliseconds, now)

		interval := intervals[r.Intn(len(intervals))]
		time.Sleep(time.Duration(interval) * time.Second)
	}
}
