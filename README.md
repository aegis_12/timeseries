
## Generate dumb data on /tmp/timeseries_test.db

```
$ cd api/dumb
$ ./main
```

## Api rest to read dumb data

```
$ cd api
$ ./main
```

## Execute frontend

```
$ npm install
$ npm run-script start
```
